﻿Shader "Unlit/SimpleShader2"
{
    Properties
    {
		_ClrA("ColorA" , Color) = (1,1,1,1)
		_ClrB("ColorB" , Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" } //有試過RenderType寫Transparent，但好像還是無法弄出透明感
		LOD 100

		Pass
		{
			 CGPROGRAM //宣告從現在開始，我們撰寫的是CG程式碼
			#pragma vertex vert //宣告你的頂點渲染器(Vertex Shader)叫啥名字
			#pragma fragment frag //宣告你的平面渲染器(Fragment Shader)叫啥名字
			//這邊取什麼，你下面的function就得取那個名字

			#include "UnityCG.cginc" //引入函式庫

			//代表著你想要取出MeshData中的哪些部分，諸如頂點位置(Vertex position)
			//或UV(二維座標，有點像是表面皮膚攤開……吧)
			struct VertexInput //原本叫appdata，但是VertexInput會比較直觀
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct VertexOutput
            {
                float2 uv : TEXCOORD0;
                /*UNITY_FOG_COORDS(1)*/
                float4 vertex : SV_POSITION;
				float3 worldPos:WORLD_POSITION;
            };

            sampler2D _MainTex;
            //float4 _MainTex_ST;
			float4 _ClrA;
			float4 _ClrB;
			uniform float3 _MousePos; //有點像是把這個設成全域變數

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				//將自己身上頂點轉換到世界座標 (頂點乘上ObjectToWorld的矩陣)
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

			float4 MyLerp(float4 a, float4 b, float t) //試著在Shader中加入function
			{
				//return t * b + (1.0 - t) * a; //講者寫的，我習慣用下面的寫法
				return a + (b - a) * t;
			}
			float3 MyLerp(float3 a, float3 b, float t)
			{
				return a + (b - a) * t;
			}
			//輸入value值，回傳該value介在a和b之間的幾%位置
			float InverseLerp(float a, float b, float value)
			{
				return (value-a) / (b-a);
			}
			float Layered(float steps, float value)
			{
				return floor(value * steps) / steps;
			}

            fixed4 frag (VertexOutput i) : SV_Target
            {
                //// sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);
                //// apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
				//上面以及前面其他預設的FOG呼叫(?)先關掉
			//float output = frac(_Time.y); 
			//對時間的秒數做frac(這個動作會使其扣掉自己目前的floor值，例如1.5的floor為1)

			//float3 textureClr = tex2D(_MainTex, i.uv);
			//根據帶入的UV點取得Texture(貼圖)對應點位的顏色
			//float3 outputClr = textureClr;
			//outputClr.y = ((sin(textureClr.x * 5 + _Time.y * 2) + 1) * 0.5);
			//做出類似波動的效果
			//outputClr = ((sin(i.uv.x * _Time.y) + 1) * 0.5).xxx;
			//return float4(outputClr,1);

			//	/*float dist = distance(_MousePos , i.worldPos);
			//	dist = mul(dist, 4);
			//	return float4(dist.xxx,1);*/	

				float2 uv = i.uv;

				float finalT = InverseLerp(0.25, 0.75, uv.y); //在此處這麼做的話，T值在y<=0.25前都會是0，>=0.75後都是1
				//float finalT = floor(uv.y * 4) / 4;
				//return float4(finalT.xxx, 1);

				float4 clrOutput = MyLerp(_ClrA, _ClrB, finalT);
                return float4(clrOutput);
            }
            ENDCG
        }
    }
}
