﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Sample"
{
		Properties
		{
			//紋理，第一個參數為她在Inspector那邊顯示的名字
			//第二個則是型態
			//最前面的"_Texture為程式碼裡呼叫時用的名字"
			_Texture("Texture" , 2D) = "white"{}
			_Color("Color" , Color) = (1,1,1,1)

		}
		SubShader
		{
			/*Queue為渲染順序
					RenderType為渲染類型(Opaque為不透明體)
			*/
			Tags{"Queue" = "Geometry" "RenderType" = "Opaque"}
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				//定義一個Vertext Shader，所使用的方法為vert
				#pragma fragment frag

				sampler2D _Texture;
				fixed4 _Color;
				//fixed是精確度比較小浮點數，後面4代表四個 (用以表示顏色的RGBA)

				struct appdata
				{
					//取得模型位置
					fixed4 vertex : POSITION;
					//取得紋理的第一個座標
					fixed2 uv : TEXCOORD0;
				};

				struct v2f
				{
					fixed4 vertex : SV_POSITION;
					fixed2 uv : TEXCOORD0;
				};

				v2f vert(appdata i)
				{
					v2f o;
					o.uv = i.uv;
					o.vertex = UnityObjectToClipPos(i.vertex);
					return o;
				}
				fixed4 frag(v2f v):SV_Target
				{
					return tex2D(_Texture , v.uv) * _Color;
				}
				ENDCG
			}
		}
}