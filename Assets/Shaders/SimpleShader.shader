﻿Shader "Unlit/SimpleShader" //Shader的路徑名稱(建立時看到的)
{
    Properties
    {
		_Color("Color" , Color) = (1,1,1,0)
		_Gloss("Gloss" , float ) = 1
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
		
        Pass
        {
            CGPROGRAM //宣告從現在開始，我們撰寫的是CG程式碼
            #pragma vertex vert //宣告你的頂點渲染器(Vertex Shader)叫啥名字
            #pragma fragment frag //宣告你的平面渲染器(Fragment Shader)叫啥名字
			//這邊取什麼，你下面的function就得取那個名字

            #include "UnityCG.cginc" //引入函式庫
			#include "Lighting.cginc"
			#include "AutoLight.cginc" //講者不太確定哪一個，但其中一個可以提供處理光照的方法

			//代表著你想要取出MeshData中的哪些部分，諸如頂點位置(Vertex position)
			//或UV(二維座標，有點像是表面皮膚攤開……吧)
            struct VertexInput //原本叫appdata，但是VertexInput會比較直觀
            {
                float4 vertex : POSITION;
				float2 uv0 : TEXCOORD0;
				float3 normal:NORMAL;
				//normal在這邊是頂點各自的法線向量 (有點像是那個點朝外看向哪個方向)
				//然後頂點向量是會Normalized的(長度變1)
            };

            struct VertexOutput //原本叫v2f，其實是指交由VertexShader算完後，再傳給FragmentShader的資料
            {
                float4 clipSpacePos : SV_POSITION ;
				//原本叫vertex(頂點)，但是比較精確來說是clipSpacePos，也就是物體上的頂點投射到畫面上的座標
				float2 uv0 : TEXCOORD0;
				float3 normal:NORMAL; //原本好像得要去抓TEXCOORD1，但現在看改成NORMAL似乎也行
				float3 worldPos:WORLD_POSITION; //世界座標，好像要先去抓TEXCOORD2
				//2020/2/23 經測試發現，VertexOutput(v2f)這邊:後面的名字其實可以隨便取，只要不重複就好
            };

            //sampler2D _MainTex;
            //float4 _MainTex_ST;
			float4 _Color;
			//特別注意，你不能只是在Properties那邊宣告了_Color後就直接使用，到了Pass裡面還要再次宣告
			float _Gloss;

			//這是你的VertexShader
            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
				o.uv0 = v.uv0;
				o.normal = v.normal;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				//將自己身上頂點轉換到世界座標 (頂點乘上ObjectToWorld的矩陣)
                o.clipSpacePos = UnityObjectToClipPos(v.vertex); 
				//後面那個function會將物件身上的頂點轉換成投射至畫面的座標
                return o;
            }
			
			float Layered(float steps, float value)
			{
				return floor(value * steps) / steps;
			}

			//這是你的FragmentShader(平面渲染器，也就是最後投影到畫面上的樣子)
            fixed4 frag (VertexOutput o) : SV_Target
            {
				float2 uv = o.uv0;
				//return float4(o.worldPos, 1);
				float3 normal = normalize(o.normal);
				//事先將法線向量單位化(因為頂點間有時會取得長度<1的向量)
				//return float4(1,1,1,1);//這邊有點像是每個點要輸出什麼，直接回傳float4(1,1,1,0)相當於回傳白色
				//return float4(uv.xxx, 0); //這一個是按照UV的X軸去染色0的時候為黑，1的時候為白
				//return float4(normal,0); //按照每個頂點的法線向量上色
				//return float4(normal * 0.5 + 0.5, 0);
				//將頂點向量的數值限縮在0~1之間 
				//(原本會是-1~1之間，使的一部份的顏色都是黑的)

				//這裡試著加入光照源
				//float3 lightSrc = normalize(float3(1, 1, 1)); //光從哪打來
				float3 lightSrc = _WorldSpaceLightPos0.xyz; //改取場景中的光源
				float lightFalloff = max( 0 , dot(lightSrc, normal));
				//這有點難講，dot算是取兩個向量的重疊度(大概)，例如正面(完全重疊)的話就會是1反面則是-1
				//lightFalloff = step(0.1, lightFalloff);
				//若lightFalloff低於0.1，則回傳0，否則回傳1，藉此做出類似卡通風格的渲染				
				//float3 lightClr = float3(0.8, 0.7, 0.6); //原本是自己宣告光源顏色
				float3 lightClr = _LightColor0.rgb; //改取場景中的光源顏色
				float3 directDiffuseLight = lightClr * lightFalloff; 
				//光源顏色乘上光打在這個點上的亮度 (也就是這個點上的光照顏色)

				//自己加上的環境光色，會均勻地散布在整個物體表面
				float3 ambientLight = float3(0.35, 0.15, 0.15); 
				
				//算出與相機的距離(或著該稱位置差異量)，並搭配光源計算出表面光澤的感覺
				float3 camPos = _WorldSpaceCameraPos;//內建變數，取得Camera的World位置
				float3 fragToCam = camPos - o.worldPos;
				float3 viewDir = normalize(fragToCam);
				float3 viewReflect = reflect(-viewDir, normal);//取得反射後的向量，參數為(射進去的向量，法線向量)
				float specularFalloff = max(0, dot(viewReflect, lightSrc));
				//取得反射率遞減後的值
				specularFalloff = pow(specularFalloff, _Gloss);//之後再去跟光澤度做計算
				//specularFalloff = Layered(3, specularFalloff); //製造分層效果
				//specularFalloff = step(0.1, specularFalloff);
				float3 directSpecular = specularFalloff * lightClr; //計算高光
				//return float4(specularFalloff.xxx, 0);

				float3 diffuseLight = ambientLight + directDiffuseLight;
				float3 finalClr = diffuseLight * _Color.rgb +  directSpecular;//最終將環境與光源顏色乘上自己本身的顏色

				//return float4(lightFalloff.xxx, 0); //單純取得各個點的光照值
				return float4(finalClr, 0);
            }
            ENDCG //CG程式碼結束
        }
    }
}
