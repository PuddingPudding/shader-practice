﻿Shader "Unlit/MyFresnel"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {} //包在身上的圖案
		_MainTex("Albedo", 2D) = "white" {}
		_HighlightClr("HighlightClr" , Color) = (1,1,1,1)
		_FresnelPwr("FresnelPower" , float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			//#include "Lighting.cginc"
			//#include "AutoLight.cginc" //講者不太確定哪一個，但其中一個可以提供處理光照的方法

            //代表著你想要取出MeshData中的哪些部分，諸如頂點位置(Vertex position)
			//或UV(二維座標，有點像是表面皮膚攤開……吧)
            struct VertexInput //原本叫appdata，但是VertexInput會比較直觀
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal:NORMAL;
            };

            struct VertexOutput
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float3 normal:NORMAL;
				float3 worldPos:WORLD_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _HighlightClr;
			float _FresnelPwr;

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal =mul(unity_ObjectToWorld , v.normal); //好像模型的法線向量可能會跟世界座標上的不一樣，所以需要做轉換
				//o.normal = v.normal;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				//將自己身上頂點轉換到世界座標 (頂點乘上ObjectToWorld的矩陣)
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (VertexOutput vOutput) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, vOutput.uv);
                // apply fog
                UNITY_APPLY_FOG(vOutput.fogCoord, col);

				float3 normal = normalize(vOutput.normal);//取得法線向量
				float3 camPos = _WorldSpaceCameraPos;//內建變數，取得Camera的World位置
				
				//先用"朝頂點看"的做法
				//float3 viewDir = vOutput.worldPos - camPos; //從攝影機看向頂點的向量
				//viewDir = normalize(viewDir);
				//float3 reflectAngle = reflect(viewDir, normal);
				//float fresnelValue = 1 - max(0, dot(reflectAngle, normal));

				//以下用 "頂點看向攝影機" 的做法 (上下差不多，不過下面可以少一行)
				float3 viewSrcDir = camPos - vOutput.worldPos; //頂點看向攝影機
				viewSrcDir = normalize(viewSrcDir);
				float fresnelValue = 1 - max(0, dot(viewSrcDir, normal));

				fresnelValue = pow(fresnelValue, _FresnelPwr);
				float4 fresnelClr = _HighlightClr * fresnelValue;

                return (col *(1-fresnelValue) ) + fresnelClr;
            }
            ENDCG
        }
    }
}
