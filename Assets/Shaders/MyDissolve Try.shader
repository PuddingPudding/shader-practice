﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/MyDissolve Try"
{
    Properties
    {
		_MainTex("Main Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		//Toggle標籤會讓該屬性在Insepector當中變得像布林值一樣可以勾選，勾起來代表1，空著則代表0
		//在程式碼裡頭可以用屬性_ON來判定他是否有勾選(以這邊為例的話就是PixelSnap_ON)
		_EdgeColour1("Edge colour 1", Color) = (1.0, 1.0, 1.0, 1.0)
		_EdgeColour2("Edge colour 2", Color) = (1.0, 1.0, 1.0, 1.0)
		_Level("Dissolution level", Range(0.0, 1.0)) = 0.1
		_Edges("Edge width", Range(0.0, 1.0)) = 0.1

			//原本下面的是要測試ZTest，但好像沒啥用
		_NormalColor("Normal Color", Color) = (1, 1, 1, 1)
		_ZTestColor("ZTest Color", Color) = (0, 0, 1, 1)
    }
    SubShader
    {
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
			//常見的混和(Blend)模式，由現在(此頂點的顏色*Alpha值) + (後面其他點顏色*1-Alpha值)算出
			Cull Off		//關閉汰除(平常是開著的，代表他只會渲染玩家看到的這一面)
			Lighting Off //關閉打光
			ZWrite On //深度寫入(Off的話有時會讓你從前面看到後面的東西)
			Fog { Mode Off }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile __ PIXELSNAP_ON
			//有點像是定義可開關的變數，透過其他程式來控制說要切換成哪個模式，如果都沒有選的話則會預設用第一個
			//(第一個也可以直接取"__"，這樣的話可以少用一個宣告空間，另外我不確定為什麼，當我把前面射程Local的時候就會沒用)
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 snapClr:Color;
            };

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			float4 _EdgeColour1;
			float4 _EdgeColour2;
			float _Level;
			float _Edges;
			float4 _MainTex_ST;

			//原本要測試ZTest，但好像沒啥用
			uniform float4 _ZTestColor;
			uniform float4 _NormalColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);

				o.snapClr = float4(0, 0, 1, 1); //實驗說到底有沒有開啟PIXELSNAP

				#ifdef PIXELSNAP_ON
					o.vertex = UnityPixelSnap(o.vertex); //2020.03.03感覺不太出這一行有什麼用 (好像是保留像素點)
					//o.vertex += float4(2, 0, 0, 0); //雖然只是實驗一下，但發現可以用這個方式把整個物體渲染的位置往右調整
					//o.vertex = float4(0, 0, 0, 0);
					//o.uv = float2(0, 0);
					o.snapClr = float4(1, 0, 0, 1); //實驗說到底有沒有開啟PIXELSNAP
				#endif

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				float cutout = tex2D(_NoiseTex, i.uv).x; //取出該頂點在原本雜訊圖案中擁有的R值

				/*if (cutout < _Level)//2020.03.03 據說if判斷在Shader裡面很吃資源，所以我想說改用下面的寫法來避免
					col = float4(0, 0, 0, 0);*/
				col.a = step(_Level, cutout); //若後者>=前者則回傳1，否則回傳0

				if (cutout < col.a && cutout < _Level + _Edges)
					col = lerp(_EdgeColour1, _EdgeColour2, (cutout - _Level) / _Edges);

				return col;
            }
            ENDCG
        }		
    }
}
