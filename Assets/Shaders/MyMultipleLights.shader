﻿Shader "Unlit/MyMultipleLights"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Gloss("Gloss", Range(8.0,256)) = 20
		_DiffuseClr("Diffuse Color", Color) = (1,1,1,1)
		_SpecularClr("Specular Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		//Opaque: 用於大多數著色器（法線著色器、自發光著色器、反射著色器以及地形的著色器）。
		//Transparent : 用於半透明著色器（透明著色器、粒子著色器、字體著色器、地形額外通道的著色器）。
		//TransparentCutout : 蒙皮透明著色器（Transparent Cutout，兩個通道的植被著色器）。
		//Background : Skybox shaders.天空盒著色器。
		//Overlay : GUITexture, Halo, Flare shaders.光暈著色器、閃光著色器
		Tags {"RenderType" = "Opaque" }
		LOD 100

		//Base Pass
		Pass
		{
			//用於向前渲染，該Pass會計算環境光，最重要的平行光，逐頂點/SH光源和LightMaps
			//Tags{"LightMode" = "ForwardBase"}
			//2020.03.04 發現上面的Pass標籤加了就會怪怪的 (外面沒看到錯誤訊息，但你的物體會整個變紫色)

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			//multi_compile_fwdbase指令可以保證我們在shader中使用光照衰減等光照變量可以被正確賦值
			//#pragma multi_compile_fwdbase
			#include "Lighting.cginc"

			float _Gloss;
			float4 _DiffuseClr;
			float4 _SpecularClr;

			struct VertextInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				float3 worldNormal : TEXCOORD1;
			};

			v2f vert(VertextInput v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				//獲取世界座標下的法線的單位向量
				fixed3 worldNormal = normalize(i.worldNormal);
				//獲取世界座標下的光照方向的單位向量
				fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
				//獲取世界座標下的視角方向的單位向量
				fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
				//獲取環境光
				//fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz;
				//漫反射光的計算公式
				fixed3 diffuse = _LightColor0.rgb * _DiffuseClr.rgb * max(0, dot(worldNormal, worldLightDir));
				//環境光的計算公式
				fixed3 halfDir = normalize(worldLightDir + worldViewDir);
				fixed3 specular = _LightColor0.rgb * _SpecularClr.rgb * pow(max(0, dot(worldNormal, halfDir)), _Gloss);

				//平行光的距離永遠爲1,不存在光照衰減
				float atten = 1.0;

				return fixed4(/*ambient +*/ (diffuse + specular) * atten, 1.0);
		}
		ENDCG
	}
	//Addtional Pass
	Pass
	{
			//用於向前渲染，該模式代表除場景中最重要的平行光之外的額外光源的處理，每個光源會調用該pass一次
			/*Tags{"LightMode" = "ForwardAdd"}*/

			//混合模式，表示該Pass計算的光照結果可以在幀緩存中與之前的光照結果進行疊加，否則會覆蓋之前的光照結果
			Blend One One

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			//multi_compile_fwdadd指令可以保證我們在shader中使用光照衰減等光照變量可以被正確賦值
			#pragma multi_compile_fwdadd
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			float _Gloss;
			float4 _DiffuseClr;
			float4 _SpecularClr;

			struct VertextInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				float3 worldNormal : TEXCOORD1;
			};

			v2f vert(VertextInput v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				return float4(0,0,0,1);
				//獲取世界座標下的法線的單位向量
				fixed3 worldNormal = normalize(i.worldNormal);
			//獲取世界座標下的視角方向的單位向量
			fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));

				#ifdef USING_DIRECTIONAL_LIGHT  //平行光下可以直接獲取世界座標下的光照方向
					fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz);
				#else  //其他光源下_WorldSpaceLightPos0代表光源的世界座標，與頂點的世界座標的向量相減可得到世界座標下的光照方向
					fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos.xyz);
				#endif

				//漫反射光的計算公式
				fixed3 diffuse = _LightColor0.rgb * _DiffuseClr.rgb * max(0, dot(worldNormal, worldLightDir));

				//環境光的計算公式
				fixed3 halfDir = normalize(worldLightDir + worldViewDir);
				fixed3 specular = _LightColor0.rgb * _SpecularClr.rgb * pow(max(0, dot(worldNormal, halfDir)), _Gloss);

				#ifdef USING_DIRECTIONAL_LIGHT  //平行光下不存在光照衰減，恆值爲1
					fixed atten = 1.0;
				#else
					#if defined (POINT)    //點光源的光照衰減計算
				//unity_WorldToLight內置矩陣，世界座標到光源空間變換矩陣。與頂點的世界座標相乘可得到光源空間下的頂點座標
				float3 lightCoord = mul(unity_WorldToLight, float4(i.worldPos, 1)).xyz;
				//利用Unity內置函數tex2D對Unity內置紋理_LightTexture0進行紋理採樣計算光源衰減，獲取其衰減紋理，
				//再通過UNITY_ATTEN_CHANNEL得到衰減紋理中衰減值所在的分量，以得到最終的衰減值
				fixed atten = tex2D(_LightTexture0, dot(lightCoord, lightCoord).rr).UNITY_ATTEN_CHANNEL;
			#elif defined (SPOT)   //聚光燈的光照衰減計算
				float4 lightCoord = mul(unity_WorldToLight, float4(i.worldPos, 1));
				//(lightCoord.z > 0)：聚光燈的深度值小於等於0時，則光照衰減爲0
				//_LightTextureB0：如果該光源使用了cookie，則衰減查找紋理則爲_LightTextureB0
				fixed atten = (lightCoord.z > 0) * tex2D(_LightTexture0, lightCoord.xy / lightCoord.w + 0.5).w * tex2D(_LightTextureB0, dot(lightCoord, lightCoord).rr).UNITY_ATTEN_CHANNEL;
			#else
				fixed atten = 1.0;
			#endif
		#endif

				//這裏不再計算環境光，在上個Base Pass中已做計算
				return fixed4((diffuse + specular) * atten, 1.0);
			}

			ENDCG
		}
	}
	FallBack "Unlit/MyDissolve Try" //假如無法運作的話，那就去找另一個Shader
}
