﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseGlowing : MonoBehaviour
{
    [SerializeField]
    private Camera m_mainCam;
    [SerializeField]
    private GameObject m_glowingObj;
    [SerializeField]
    private Material m_myMaterial;
    [SerializeField]
    private Shader m_shader;
    [SerializeField]
    private MeshRenderer m_meshRenderer;
    // Start is called before the first frame update
    void Start()
    {
        m_mainCam = (m_mainCam) ? Camera.main : m_mainCam;

        //以下試著在執行期間生成新的Material，但好像失敗了
        m_myMaterial = new Material(m_shader);
        //m_myMaterial.SetColor("Color" , Color.green);
        m_meshRenderer.material = m_myMaterial;
        //m_meshRenderer.material.color = Color.green;
        m_meshRenderer.material.SetColor("_ClrA", Color.green);
        m_meshRenderer.material.SetColor("_ClrB", Color.yellow);
        //m_meshRenderer.material.SetFloat("_Gloss", 32); //後來用漸層的那個shader，就沒有Gloss值
        //後來才成功，似乎得先把Material指派到Renderer上之後才會有效
    }

    // Update is called once per frame
    void Update()
    {
        Plane p = new Plane(m_glowingObj.transform.forward
            , m_glowingObj.transform.position); //製造一個平面，參數分別為(法向量，射入點)
        Vector2 mousePos = Input.mousePosition;
        Ray ray = m_mainCam.ScreenPointToRay(mousePos); //從畫面上滑鼠的點射入一道雷射
        if(p.Raycast(ray , out float enterDist)) //若有擊中平面，則進行以下動作
        {
            Vector3 worldMousePos = ray.GetPoint(enterDist);
            Shader.SetGlobalVector("_MousePos", worldMousePos);
        }
    }
}
